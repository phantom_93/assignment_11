﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPG_Character;
using RPG_Character.Characters;
using RPG_Character.Weapons;
using RPG_Character_GUI;

// this is for creating window
namespace RPG_Character_GUI
{
    public partial class CreateForm : Form
    {
        public event EventHandler onEntityCreate;
        #region onEntityCreated 
        protected virtual void onEntityCreated(EventArgs e)
        {
            EventHandler handler = onEntityCreate;
            handler?.Invoke(this, e);
        }
        #endregion  
        public CreateForm()
        {
            InitializeComponent();
            // eventhandler when a newindex changes at the combo box  
            comboBox1.SelectedIndexChanged += ComboBox1_SelectedIndexChanged;
        }


        #region comboBox 
        /*
         * Retrieves the newly selected index from the combobox
         * Clears the entity type combo box and lists either Character or Weapon types
         *      that can be created
         *      
         * tl;dr: Retrieve the combobox1 index, get all types(character/weapon), display them in combobox2
         */
        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = (sender as ComboBox).SelectedIndex;
            Type entityType = Entity.EntityType[index];

            comboBox2.Items.Clear();

            Type[] types = null;
            switch (entityType.Name)
            {
                case "Character":
                    types = Character.DefinedCharacters;
                    break;
                case "Weapon":
                    types = Weapon.DefinedWeapons;
                    break;
            }

            foreach (Type type in types)
                comboBox2.Items.Add(type.Name);
            comboBox2.SelectedIndex = 0;
        }
        #endregion


        #region ShowCreate method
        /*
         * Displays the creation form as a dialog
         * 
         * Resets all controls
         * Fills combobox1 with entity types
         * Finally displays the dialog
         */
        public void ShowCreate()
        {
            comboBox1.Items.Clear();
            comboBox2.Items.Clear();
            textBox1.ResetText();
            // add items to comboBox1 
            foreach (Type entityType in Entity.EntityType)
                comboBox1.Items.Add(entityType.Name);
            comboBox1.SelectedIndex = 0;

            ShowDialog();
        }
        #endregion 

        #region createButton 
        private void createBtn_Click(object sender, EventArgs e)
        {
            // Get the entity category type
            int selectedEntityCategory = comboBox1.SelectedIndex;
            Type entityCategory = Entity.EntityType[selectedEntityCategory];
            
            // Get the index of an entity type (i.e. from either DefinedCharacters or DefinedWeapons)
            int selectedEntityType = comboBox2.SelectedIndex;

            Type entityType = default;
            /*if the user has chosen character, then the user will be able to choose 9 characters 
             if the user has chosen Weapn, then the user will be abel to choose 5 different weapons*/
            switch(entityCategory.Name)
            {
                case "Character":
                    entityType = Character.DefinedCharacters[selectedEntityType];
                    break;
                case "Weapon":
                    entityType = Weapon.DefinedWeapons[selectedEntityType];
                    break;
            }

            string entityName = textBox1.Text;
            // if the user has not chosen any choice, show this message 
            if (entityName.Length == 0)
            {
                MessageBox.Show("Please specify an entity name.");
                return;
            }

            // Instantiate the entity and insert it into the database (if the entity variable ins't null of course)
            Entity entity = null;
            switch (entityCategory.Name)
            {
                case "Character":
                    entity = CharCrud.Create(entityType.Name, entityName) as Character;
                    break;
                case "Weapon":
                    entity = WeaponCrud.Create(entityType.Name, entityName) as Weapon;
                    break;
            }

            

            Close();
            onEntityCreate(entity, null); // Fires the 'onEntityCreate' event, passing the newly created entity as the sender
        }
        #endregion

        #region cancel button
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            // close the window
            Close();
        }
        #endregion 
    }
}
