﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPG_Character;
using RPG_Character.Characters;
using System.IO;
using CsvHelper;
using System.Globalization;
using RPG_Character.Weapons;

namespace RPG_Character_GUI
{
    public partial class Form1 : Form
    {
        Dictionary<string, List<Entity>> entities;

        Dictionary<TreeNode, Entity> clickableNodes;
        // two TreeNode objects are created; one for charactes and another for Weapons
        TreeNode charCatNode = new TreeNode("Characters");
        TreeNode weapCatNode = new TreeNode("Weapons");


        Entity selectedEntity = null; 
        CreateForm creationForm;

        public Form1()
        {
            InitializeComponent();

            clickableNodes = new Dictionary<TreeNode, Entity>();
            RefreshTree(); // Refresh the tree view with updated entity information

            // Bind UI event handlers to methods
            treeView1.NodeMouseClick += onEntityClick;
            createBtn.Click += CreateBtn_Click;
            exportBtn.Click += ExportBtn_Click;

            // Instantiate the creation form and bind its event
            creationForm = new CreateForm();
            creationForm.onEntityCreate += CreationForm_onEntityCreate;

         
        }

        #region export button
        private void ExportBtn_Click(object sender, EventArgs e)
        {
            List<Character> characters = new List<Character>();
            List<Weapon> weapons = new List<Weapon>();
            // read all characters from database 
            foreach (KeyValuePair<string, List<Character>> charList in CharCrud.ReadAll())
                foreach (Character character in charList.Value)
                    characters.Add(character);

            // read all weapons from database 
            foreach (KeyValuePair<string, List<Weapon>> weaponList in WeaponCrud.ReadAll())
                foreach (Weapon weapon in weaponList.Value)
                    weapons.Add(weapon);

            string appDir = Directory.GetCurrentDirectory(); // gets the current directory 

            MessageBox.Show($"Exporting CSV to '{appDir}'");
            // wirte data from database and export to CSV file 
            using (StreamWriter writer = new StreamWriter($"{appDir}\\export.csv"))
            {
                using (CsvWriter csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    // Write characters
                    csv.WriteHeader(typeof(Character));
                    csv.NextRecord();
                    csv.WriteRecords(characters);
                    // Write weapons
                    csv.NextRecord();
                    csv.WriteHeader(typeof(Weapon));
                    csv.NextRecord();
                    csv.WriteRecords(weapons);
                }
            }
        }
        #endregion

        #region CreationForm_onEntityCreate
        private void CreationForm_onEntityCreate(object sender, EventArgs e)
        {
            
            // once the entity is created, refresh the tree and clear the overview display
            RefreshTree();
            ResetDisplay();
        }
        #endregion 


        #region create button
        private void CreateBtn_Click(object sender, EventArgs e)
        {
            // Displays the creation interface
            creationForm.ShowCreate();
        }
        #endregion


        #region refreshtree method
        /*
         * RefreshTree function 
         * clear nodes 
         * read from the databases 
         * add characters and weapons to their respective trees 
         * view them
         */
        private void RefreshTree()
        {
            treeView1.Nodes.Clear();
            charCatNode.Nodes.Clear();
            weapCatNode.Nodes.Clear();
            clickableNodes.Clear();

            Dictionary<string, List<Character>> characterDb = CharCrud.ReadAll();
            Dictionary<string, List<Weapon>> weaponDb = WeaponCrud.ReadAll();

            // Add all characters
            foreach (Type charType in Character.DefinedCharacters)
            {
                string[] typeArr = charType.ToString().Split('.');
                string typeName = typeArr[typeArr.Length - 1];

                TreeNode node = new TreeNode(typeName);

                List<Character> characters = characterDb[typeName];
                foreach(Character character in characters)
                {
                    TreeNode charNode = new TreeNode(character.Name);

                    clickableNodes.Add(charNode, character);
                    node.Nodes.Add(charNode);
                }

                charCatNode.Nodes.Add(node);
            }

            // Add all weapons
            foreach (Type weaponType in Weapon.DefinedWeapons)
            {
                string[] typeArr = weaponType.ToString().Split('.');
                string typeName = typeArr[typeArr.Length - 1];

                TreeNode node = new TreeNode(typeName);

                List<Weapon> weapons = weaponDb[typeName];
                foreach (Weapon weapon in weapons)
                {
                    TreeNode weaponNode = new TreeNode(weapon.Name);

                    clickableNodes.Add(weaponNode, weapon);
                    node.Nodes.Add(weaponNode);
                }

                weapCatNode.Nodes.Add(node);
            }

            treeView1.Nodes.Add(charCatNode);
            treeView1.Nodes.Add(weapCatNode);
            treeView1.ExpandAll();
            treeView1.Nodes[0].EnsureVisible();
        }
        #endregion 


        #region onEntityClick
        /*
         * Once an entity is created
         * Display the entity in the tree view and in the overview
         */
        private void onEntityClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode node = e.Node;
            
            if (clickableNodes.ContainsKey(node))
            {
                Entity entity = clickableNodes[node];
                DisplayEntity(entity);
            }
        }
        #endregion

        #region DisplayEntity
        /*
         * Logic used to display information for an entity
         * on the main form
         */
        private void DisplayEntity(Entity entity)
        {
            selectedEntity = entity;

            string type = entity.GetType().ToString().Split('.')[1];
            type = type.Substring(0, type.Length - 1);

            switch(type)
            {
                case "Character":
                    Character selectedCharacter = entity as Character;

                    textBox1.Text = selectedCharacter.Name;
                    numericUpDown1.Value = selectedCharacter.Health;
                    numericUpDown2.Value = selectedCharacter.Intellect;
                    numericUpDown3.Value = selectedCharacter.Energy;
                    numericUpDown4.Value = selectedCharacter.Stamina;
                    numericUpDown5.Value = selectedCharacter.Agility;
                    numericUpDown6.Value = selectedCharacter.Armor;
                    numericUpDown7.Value = selectedCharacter.HitPoints;
                    break;
                case "Weapon":
                    Weapon selectedWeapon = entity as Weapon;
                    textBox2.Text = selectedWeapon.Name;
                    numericUpDown8.Value = selectedWeapon.Damage;
                    numericUpDown9.Value = selectedWeapon.Capability;
                    break;
            }
        }
        #endregion

        #region ResetDisplay
        /*
         * Reset all controls to their default setting
         * Deselect any selected entity and unselect it in the tree view
         */
        private void ResetDisplay()
        {
            selectedEntity = null;
            treeView1.SelectedNode = null;

            textBox1.ResetText();
            numericUpDown1.ResetText();
            numericUpDown2.ResetText();
            numericUpDown3.ResetText();
            numericUpDown4.ResetText();
            numericUpDown5.ResetText();
            numericUpDown6.ResetText();
            numericUpDown7.ResetText();
            numericUpDown8.ResetText();
            numericUpDown9.ResetText();
        }
        #endregion

        #region Refresh Button
        private void refreshBtn_Click(object sender, EventArgs e)
        {
            // refresh the treeView 
            RefreshTree();
        }
        #endregion

        #region exit button
        private void exitBtn_Click(object sender, EventArgs e)
        {
            // exit from the application 
            Application.Exit();
        }
        #endregion

        #region delete button
        private void deleteBtn_Click(object sender, EventArgs e)
        {
            // delete entity
            if (selectedEntity == null)
            {
                MessageBox.Show("Unable to delete, no entity selected.");
                return;
            }

            string type = selectedEntity.GetType().ToString().Split('.')[1];
            type = type.Substring(0, type.Length - 1);

            switch (type)
            {
                case "Character":
                    Character character = selectedEntity as Character;
                    CharCrud.Delete(character.GetType().Name, character);
                    break;
                case "Weapon":
                    Weapon weapon = selectedEntity as Weapon;
                    WeaponCrud.Delete(weapon);
                    break;
            }

            ResetDisplay();
            RefreshTree();
        }
        #endregion

        #region update button
        private void updateBtn_Click(object sender, EventArgs e)
        {
            if (selectedEntity == null)
            {
                MessageBox.Show("Unable to update, no entity selected.");
                return;
            }

            string type = selectedEntity.GetType().ToString().Split('.')[1];
            type = type.Substring(0, type.Length - 1);

            switch (type)
            {
                case "Character":
                    Character character = selectedEntity as Character;
                    Character charB = Activator.CreateInstance(character.GetType(), Name) as Character;

                    string name = textBox1.Text;
                    int health = Convert.ToInt32(numericUpDown1.Value);
                    int intellect = Convert.ToInt32(numericUpDown2.Value);
                    int energy = Convert.ToInt32(numericUpDown3.Value);
                    int stamina = Convert.ToInt32(numericUpDown4.Value);
                    int agility = Convert.ToInt32(numericUpDown5.Value);
                    int armor = Convert.ToInt32(numericUpDown6.Value);
                    int hitpoints = Convert.ToInt32(numericUpDown7.Value);

                    charB.ID = character.ID;
                    charB.Health = health;
                    charB.Intellect = intellect;
                    charB.Energy = energy;
                    charB.Stamina = stamina;
                    charB.Agility = agility;
                    charB.Armor = armor;
                    charB.HitPoints = hitpoints;

                    CharCrud.Update(character.GetType().Name, charB);
                    MessageBox.Show(charB.ToString());
                    if (selectedEntity.Equals(charB))
                    {
                        MessageBox.Show("Failed to update, the entity's properties have not changed");
                        return;
                    }
                    break;

                case "Weapon":
                    Weapon weapon = selectedEntity as Weapon;
                    Weapon charW = Activator.CreateInstance(weapon.GetType(), Name) as Weapon;

                    string nameW = textBox2.Text;
                    int damage = Convert.ToInt32(numericUpDown8.Value);
                    int capability = Convert.ToInt32(numericUpDown9.Value);

                    charW.ID = weapon.ID;
                    charW.Name = nameW;
                    charW.Damage = damage;
                    charW.Capability = capability;

                    WeaponCrud.Update(charW); 
                    if(selectedEntity.Equals(charW))
                    {
                        MessageBox.Show("Failed to update, the entity's properties have not changed");
                        return;
                    }
                    break;
            }
          
            RefreshTree();
            //DisplayCharacter(charB);
        }
        #endregion
    }
}
