﻿using System;
using System.Collections.Generic;
using System.Text;

using RPG_Character.Characters;
using RPG_Character.Weapons;

namespace RPG_Character
{
    public abstract class Entity
    {
        #region property
        public string Name { get; set; }
        public string Type
        {
            get {

                return GetType().Name;
            }
        }

        public static Type[] EntityType =
        {
            typeof(Character),
            typeof(Weapon)
        };
        #endregion


        #region constructor 
        public Entity(string name)
        {
            Name = name; 
        }
        #endregion
    }
}
