﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Interfaces
{
    public interface IRunner
    {
        void Run(); 
    }
}
