﻿using RPG_Character.Characters;
using RPG_Character.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Interfaces
{
    public interface IStealer
    {
        void Steal(Character character); 
    }
}
