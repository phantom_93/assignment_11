﻿using RPG_Character.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Characters
{
    public class Warlock : Wizard
    {
        #region constructor 
        public Warlock(string name) : base(name)
        {
            // can use the spell 
            // can manipulate 
        }
        public Warlock(string name, Weapon weapon) : base(name, weapon)
        {
            // can use the spell 
            // can manipulate 
        }
        #endregion

        #region methods 
        public override void Manipulate(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} can manipulate the forces of dark energy and attack{character.Name} using {weapon.Name}");
        }
        public override void Heal(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} can heal the mortal wounds, illness and take the pain away of {character.Name} using {weapon.Name}");
        }

        public override void Kill(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} butchered {character.Name} using {weapon.Name}");
        }

        #endregion
    }
}
