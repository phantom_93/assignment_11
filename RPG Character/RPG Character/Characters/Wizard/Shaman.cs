﻿using RPG_Character.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Characters
{
    public class Shaman : Wizard
    {
        #region constructor 
        public Shaman(string name) : base(name)
        {
            // can manipulate the forces of nature 
        }

        public Shaman(string name, Weapon weapon) : base(name, weapon)
        {
            // can manipulate the forces of nature 
        }
        #endregion

        #region methods 
        public override void Manipulate(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} manipulate the forces of nature in this world and unleashed attacks on{character.Name} using {weapon.Name}");
        }
        public override void Heal(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} can heal the mortal wounds, illness and take the pain away of  {character.Name} using {weapon.Name}");
        }
        public override void Kill(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} butchered {character.Name} using {weapon.Name}");
        }
        #endregion
    }

}
