﻿using RPG_Character.Interfaces;
using RPG_Character.Weapons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace RPG_Character.Characters
{
    public abstract class Wizard : Character, ICast, IUpgrade, IManipulate
    {
        #region constructors 
        public Wizard(string name) : base(name)
        {

        }
        public Wizard(string name, Weapon weapon): base(name)
        {

        }
        #endregion


        #region methods 
        public virtual void Cast(Character character, Weapon weapon) 
        {
            Console.WriteLine($"{Name} casts the spell evil curse to kill {character.Name} using {weapon.Name} mercilessly");
        }

        public virtual void Upgrade()
        {
            Console.WriteLine($"{Name} can upgrade themselves to become stronger, faster and fiercer");
        }
        public virtual void Manipulate(Character character, Weapon weapon)
        {
            Console.WriteLine($"Can manipulate the forces of nature");
        }
        public virtual void Heal(Character character, Weapon weapon)
        {
            Console.WriteLine($"Can heal the mortal wounds, illness and take the pain away");
        }
        public virtual void Kill(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} butchered {character.Name} using {weapon.Name}");
        }
        #endregion
    }
}
