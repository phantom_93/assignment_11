﻿using System;
using System.Collections.Generic;
using System.Text;
using RPG_Character.Weapons;

namespace RPG_Character.Characters
{
    public class Druid : Wizard
    {
        #region constructor
        public Druid(string name) : base(name)
        {
            // can cast the spell 
            // heal wounds and illnesses 
            // functions to make oneself faster, stronger and fiercer
        }
        public Druid(string name, Weapon weapon) : base(name, weapon)
        {
            // can cast the spell 
            // heal wounds and illnesses 
            // functions to make oneself faster, stronger and fiercer
        }
        #endregion

        #region methods 
        public override void Cast(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} casts the spell of evil curses on {character.Name} using {weapon.Name}");
        }
        public override void Manipulate(Character character, Weapon weapon)
        {
            Console.WriteLine($"manipulate the forces of nature in this world and unleash attack on {character.Name} using {weapon.Name}");
        }
        public override void Heal(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} can heal the mortal wounds, illness and take the pain away of {character.Name} using {weapon.Name}");
        }
        public override void Upgrade()
        {
            Console.WriteLine($"{Name} can upgrade themselves to become stronger, faster and fiercer");
        }
        public override void Kill(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} butchered {character.Name} using {weapon.Name}");
        }
        #endregion
    }

}
