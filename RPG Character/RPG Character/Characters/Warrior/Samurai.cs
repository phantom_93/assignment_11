﻿using RPG_Character.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Characters
{
    public class Samurai : Warrior
    {
        #region constructor
        public Samurai(string name) : base(name)
        {

        }
        public Samurai(string name, Weapon weapon) : base(name, weapon)
        {

        }
        #endregion 

        #region functions
        public override void Attack(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} attacks evil doer {character.Name} by using {weapon.Name} like a fierce storm.");
        }

        public override void Block(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} blocks attacks from nasty {character.Name} using {weapon.Name} like a mountain");
        }

        public override void Kill(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} butchered {character.Name} using {weapon.Name}");
        }

        public override void Move()
        {
            Console.WriteLine($"{Name} moves so fast");
        }


        public override void Run()
        {
            Console.WriteLine($"{Name} runs as fast as the wind towards the enemies");
        }
        #endregion

    }

}
