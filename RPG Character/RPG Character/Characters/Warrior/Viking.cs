﻿using RPG_Character.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Characters
{
    public class Viking : Warrior
    {
        #region constructor 
        public Viking(string name) : base(name)
        {

        }
        public Viking(string name, Weapon weapon) : base(name, weapon)
        {

        }
        #endregion

        #region methods 
        public override void Attack(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} attacks evil doer {character.Name} using {weapon.Name} like a fierce storm. ");
        }

        public override void Block(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} blocks attacks from nasty {character.Name} using {weapon.Name} like a mountain");
        }

        public override void Kill(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} butchered {character.Name} using {weapon.Name}");
        }

        public override void Move()
        {
            Console.WriteLine($"{Name} moves so fast");
        }



        public override void Run()
        {
            Console.WriteLine($"{Name} runs as fast as the wind towards the enemies");
        }
        #endregion

    }

}
