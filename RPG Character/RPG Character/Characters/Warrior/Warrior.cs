﻿using RPG_Character.Interfaces;
using System;
using RPG_Character.Weapons;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Characters
{
    public abstract class Warrior: Character, IAttacker, IBlocker, IMover, IRunner
    {
        #region constructor
        public Warrior(string name) : base(name)
        {

        }
        public Warrior(string name, Weapon weapon): base(name)
        {
           
        }
        #endregion


        #region methods 
        public virtual void Attack(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} attacks evil doer {character.Name} by using {weapon.Name} like a fierce storm.");
        }

        public virtual void Block(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} blocks attacks from nasty {character.Name} by using {weapon.Name} like a mountain");
        }

        public virtual void Move()
        {
            Console.WriteLine($"{Name} moves so fast");
        }


        public virtual void Run()
        {
            Console.WriteLine($"{Name} runs as fast as the wind towards the enemies");
        }

        public virtual void Kill(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} runs as fast as the wind towards the enemies and kills {character.Name} by using {weapon.Name}");
        }
        #endregion 

    }

}
