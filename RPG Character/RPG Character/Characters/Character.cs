﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using System.Linq;
using RPG_Character.Weapons;

namespace RPG_Character.Characters
{
  
    public abstract class Character : Entity
    {
        #region properties 
        // name property of the character 
        public int ID { get; set;}

        // other properties that character can have 
     

        public int Health { get; set; } // HP

        public int Intellect { get; set; }


        public int Energy{ get; set; }

       

        public int Stamina { get; set; }


        public int Agility { get; set; }

        public int Armor { get; set; }

        // attack points 
        public int HitPoints { get; set; }

        public Weapon Weapon { get; set; }
        #endregion

        public static Type[] DefinedCharacters = {
            typeof(Assassain),
            typeof(Bandit),
            typeof(Rogue),
            
            typeof(Knight),
            typeof(Samurai),
            typeof(Viking),
            
            typeof(Druid),
            typeof(Shaman),
            typeof(Warlock),
        };

        #region constructor 
        public Character(string name)
            :base(name)
        {
            // gives the level of health, energy and mana as full 100%  as default
            Health = 100;
            Intellect = 100;
            Energy = 100; 
            Stamina = 100;
            Agility = 100;
            Armor = 100;
            HitPoints = 100; 
            
        }

        public Character(string name, Weapon weapon): base(name)
        {
            Weapon = weapon; 
            Name = name;
            Health = 100;
            Energy = 100; 
        }
        #endregion

        #region method 

        public override string ToString()
        {
            return $"Name: {Name}, Health: {Health}, Intellect: {Intellect}, Energy: {Energy}, Stamina: {Stamina}, Agility: {Agility}, Armor: {Armor}, HitPoints: {HitPoints}";
        }
        #endregion
    }
}
