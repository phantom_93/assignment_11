﻿using System;
using System.Collections.Generic;
using System.Text;
using RPG_Character.Weapons;

namespace RPG_Character.Characters
{
    public class Bandit : Thief
    {
        #region constructor 
        public Bandit(string name) : base(name)
        {
            // attack
            // loot 
            // kill 
        }
        public Bandit(string name, Weapon weapon) : base(name, weapon)
        {
            // attack
            // loot 
            // kill 
        }
        #endregion

        #region methods
        public override void Attack(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} enjoyed burtal and merciless attacks on {character.Name} by using {weapon.Name} by using {weapon.Name}");
        }
        public override void Loot(Character character)
        {
            Console.WriteLine($"{Name} lives to loot from you. After I looted {character.Name}, you will be left with none.");
        }
        public override void Kill(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} kills {character.Name} with one strike by using {weapon.Name}. I will take everything from you");
        }
        #endregion
    }

}
