﻿using RPG_Character.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Characters
{
    public class Assassain : Thief
    {
        #region constructor
        public Assassain(string name) : base(name)
        {
            // killing efficiently
            // can run, jump and move with stealth.  
            // 
        }

        public Assassain(string name, Weapon weapon) : base(name, weapon)
        {
            // killing efficiently
            // can run, jump and move with stealth.  
            // 
        }
        #endregion


        #region methods 
        public override void Move()
        {
            Console.WriteLine($"Stealth is in my blood. Remember my Name {Name}.");
        }
        public override void Run()
        {
            Console.WriteLine($"{Name} moves like wind");
        }
        public override void Jump()
        {
            Console.WriteLine($"{Name} can jump from very high places quite effortlessly that it comes natural to me");
        }
        public override void Kill(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} kills {character.Name} with one strike mercilessly and brutally by using {weapon.Name}.");
        }
        #endregion
    }
}
