﻿using RPG_Character.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Characters
{
    public class Rogue : Thief
    {
        #region constructor
        public Rogue(string name) : base(name)
        {
            // can move with stealth
            // can run, jump and move with stealth
            // stealing is a piece of cake 
        }
        public Rogue(string name, Weapon weapon) : base(name, weapon)
        {
            // can move with stealth
            // can run, jump and move with stealth
            // stealing is a piece of cake 
        }
        #endregion

        #region methods 
        public override void Steal(Character character)
        {
            Console.WriteLine($"Be Warned, you won't see me coming {character.Name}");
        }
        public override void Move()
        {
            Console.WriteLine($"Stealth is in my blood. Remember my Name{Name}.");
        }
        public override void Attack(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} did sneak attack on {character.Name} by using {weapon.Name}");
        }
        public override void Kill(Character character, Weapon weapon)
        {
            Console.WriteLine($"{Name} killed ruthlessly {character.Name} by using {weapon.Name}");
        }
        public override void Run()
        {
            Console.WriteLine($"runned liked wind");
        }
        public override void Jump()
        {
            Console.WriteLine($"{Name} can jump from very high places quite effortlessly that it comes natural to me");
        }
        #endregion
    }


}
