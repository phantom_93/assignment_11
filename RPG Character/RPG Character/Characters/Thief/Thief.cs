﻿using RPG_Character.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using RPG_Character.Weapons;

namespace RPG_Character.Characters
{
    public abstract class Thief: Character, ILooter, IStealer, IAttacker, IMover, IRunner, IJumper
    {
        //public Character character; 

        #region constructor 

        public Thief(string name) : base(name)
        {

        }

        public Thief(string name, Weapon weapon) : base(name)
        {

        }
        #endregion

        #region methods 
        public virtual void Loot(Character character)
        {
            Console.WriteLine($"I live to loot from you. After I looted you, you will be left with none.");
        }

        public virtual void Attack(Character character, Weapon weapon)
        {
            Console.WriteLine($"did sneak attack on {character.Name}");
        }

        public virtual void Jump()
        {
            Console.WriteLine("I can jump from very high places quite effortlessly that it comes natural to me ");
        }

        public virtual void Steal(Character character)
        {
            Console.WriteLine("Be Warned, you won't see me coming");
        }
       
        public virtual void Move()
        {
            Console.WriteLine($"Stealth is in my blood. Remember my Name{Name}.");
        }
        public virtual void Run()
        {
            Console.WriteLine($"{Name} can run so fast");
        }

        public virtual void Kill(Character character, Weapon weapon)
        {
            Console.WriteLine($"I aim to kill with one strike. Don't Mess with me");
        }
        #endregion 

    }

}
