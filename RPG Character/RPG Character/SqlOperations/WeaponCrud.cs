﻿using RPG_Character.Weapons;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;


namespace RPG_Character
{
    public class WeaponCrud
    {
        #region ConnectionString
        // establish the connectionstring that opens the database and performs CRUD operations
        public static string ConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder
            {
                DataSource = @"PC7266\SQLEXPRESS",
                InitialCatalog = "RPG_Character",
                IntegratedSecurity = true
            };
            return builder.ConnectionString.ToString();
            #endregion
        }
        // perform crude operations such as create, read, update and delete
        #region Create 
        public static Weapon Create(string type, string name)
        {
            Weapon weapon = null;
            foreach (Type weapType in Weapon.DefinedWeapons)
            {
                string typeName = weapType.Name;

                if (typeName.ToLower().Equals(type.ToLower()))
                {
                    // Create an instance of the weapon from a type
                    weapon = Activator.CreateInstance(weapType, name) as Weapon;
                    type = typeName;
                    break;
                }
            }

            if (weapon != null)
            {
                string query = $"INSERT INTO Weapons (Name, Type, Damage, Capability) VALUES(@name, @type, @damage, @capability)";

                using (SqlConnection connection = new SqlConnection(ConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        // @name, @hp, @int, @eng, @sta, @agi, @arm, @hit
                        cmd.Parameters.AddWithValue("@name", weapon.Name);
                        cmd.Parameters.AddWithValue("@type", weapon.GetType().Name);
                        cmd.Parameters.AddWithValue("@damage", weapon.Damage);
                        cmd.Parameters.AddWithValue("@Capability", weapon.Capability); 
                        cmd.ExecuteNonQuery();
                    }

                    connection.Close();
                }
            }

            return weapon;
        }
        #endregion


        #region Read

        public static Dictionary<string, List<Weapon>> ReadAll()
        { 
            /* make dictionary of characters using the weapon type as a key
             * (example: 'Warrior' key contains a list of all Warriors)
             */
            Dictionary<string, List<Weapon>> weaponDict = new Dictionary<string, List<Weapon>>();

            // loop through all defined characters and retrieve all the data from the database
            // and add and create each dictionary elements containing all characters 
            foreach (Type weapType in Weapon.DefinedWeapons)
            {
                string typeName = weapType.Name;

                List<Weapon> weapons = Read(typeName);
                weaponDict.Add(typeName, weapons);
            }

            return weaponDict;
        }

        public static List<Weapon> Read(string type)
        {
            List<Weapon> items = new List<Weapon>();
            try
            {
                // select query from the table in the database
                string queryString = $"SELECT * FROM Weapons WHERE Type = @type";
                // open the database connection 
                using (SqlConnection sqlConnection = new SqlConnection(ConnectionString()))
                {
                    SqlCommand command = new SqlCommand(queryString, sqlConnection);
                    command.Parameters.AddWithValue("@type", type);

                    sqlConnection.Open();
                    // while the connection is open, read from the database
                    using (SqlDataReader reader = command.ExecuteReader()) 
                    {
                        while (reader.Read())
                        {
                            // weapon variable sets to empty
                            Weapon weapon = null;
                            // gets the value of a row based on the column index
                            int id = reader.GetInt32(0);
                            string name = reader.GetString(1);

                            switch (type)
                            {
                                // Types of thieves
                                case "Dragger":
                                    weapon = new Dragger(name);
                                    break;
                                case "MagicWand":
                                    weapon = new MagicWand(name);
                                    break;
                                case "Poison":
                                    weapon = new Poison(name);
                                    break;
                                case "Sword":
                                    weapon = new Sword(name);
                                    break;
                                case "ThrowingStar":
                                    weapon = new ThrowingStar(name);
                                    break; 

                            }

                            weapon.Damage = reader.GetInt32(3);
                            weapon.Capability = reader.GetInt32(4);
                            

                            if (weapon != null)
                            {
                                // if the weapon is not null, set the weapon Id and add it to the item list
                                weapon.ID = id;

                                items.Add(weapon);
                            }
                        }
                    }
                    sqlConnection.Close();
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return items;
        }

        #endregion
       

        #region Update
        public static string Update(Weapon weapon)
        {
            // update query
            string query = $"UPDATE Weapons SET Name =  @name, Damage= @damage, Capability= @capability WHERE ID = @id";

            using (SqlConnection connection = new SqlConnection(ConnectionString()))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    //update the parameters of the query in the database 
                    // @name, @hp, @int, @eng, @sta, @agi, @arm, @hit
                    cmd.Parameters.AddWithValue("@id", weapon.ID);
                    cmd.Parameters.AddWithValue("@name", weapon.Name);
                    cmd.Parameters.AddWithValue("@damage", weapon.Damage);
                    cmd.Parameters.AddWithValue("@capability", weapon.Capability); 

                    cmd.ExecuteNonQuery();
                }

                connection.Close();
            }

            return query;
        }

        #endregion


        #region Delete 
        public static void Delete(Weapon weapon)
        {
            // delete query
            string query = $"DELETE FROM Weapons WHERE ID = @id";

            using (SqlConnection connection = new SqlConnection(ConnectionString()))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    // delete the row where the ID matches in the database
                    // @name, @hp, @int, @eng, @sta, @agi, @arm, @hit
                    cmd.Parameters.AddWithValue("@id", weapon.ID);

                    cmd.ExecuteNonQuery();
                }

                connection.Close();
            }
        }
        #endregion

    }
}
