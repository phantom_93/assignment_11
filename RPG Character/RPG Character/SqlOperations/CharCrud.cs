﻿using RPG_Character.Characters;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;


namespace RPG_Character
{
    public class CharCrud
    {
        #region ConnectionString
        // establish the connectionstring that opens the database and performs CRUD operations
        public static string ConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder
            {
                DataSource = @"PC7266\SQLEXPRESS",
                InitialCatalog = "RPG_Character",
                IntegratedSecurity = true
            };
            return builder.ConnectionString.ToString(); 
            
        }
        #endregion

        // perform crude operations such as create, read, update and delete
        #region Create 
        public static Character Create(string type, string name)
        {
            Character character = null;
            foreach (Type charType in Character.DefinedCharacters)
            {
                string[] typeArr = charType.ToString().Split('.');
                string typeName = typeArr[typeArr.Length - 1];

                if (typeName.ToLower().Equals(type.ToLower()))
                {
                    // Create an instance of the character from a type
                    character = Activator.CreateInstance(charType, name) as Character;
                    type = typeName;
                    break;
                }
            }

            if (character != null)
            {
                string query = $"INSERT INTO {type} (Name, Health, Intellect, Energy, Stamina, Agility, Armor, HitPoints) VALUES(@name, @hp, @int, @eng, @sta, @agi, @arm, @hit)";

                using (SqlConnection connection = new SqlConnection(ConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        // @name, @hp, @int, @eng, @sta, @agi, @arm, @hit
                        cmd.Parameters.AddWithValue("@name", character.Name);
                        cmd.Parameters.AddWithValue("@hp", character.Health);
                        cmd.Parameters.AddWithValue("@int", character.Intellect);
                        cmd.Parameters.AddWithValue("@eng", character.Energy);
                        cmd.Parameters.AddWithValue("@sta", character.Stamina);
                        cmd.Parameters.AddWithValue("@agi", character.Agility);
                        cmd.Parameters.AddWithValue("@arm", character.Armor);
                        cmd.Parameters.AddWithValue("@hit", character.HitPoints);

                        cmd.ExecuteNonQuery();
                    }

                    connection.Close();
                }
            }

            return character;
        }
        #endregion


        #region Read

        public static Dictionary<string, List<Character>> ReadAll()
        { 
            /* make dictionary of characters using the character type as a key
             * (example: 'Warrior' key contains a list of all Warriors)
             */
            Dictionary<string, List<Character>> characterDict = new Dictionary<string, List<Character>>();

            // loop through all defined characters and retrieve all the data from the database
            // and add and create each dictionary elements containing all characters 
            foreach (Type charType in Character.DefinedCharacters)
            {
                string[] typeArr = charType.ToString().Split('.');
                string typeName = typeArr[typeArr.Length - 1];

                List<Character> characters = Read(typeName);
                characterDict.Add(typeName, characters);
            }

            return characterDict;
        }

        public static List<Character> Read(string table)
        {
            List<Character> items = new List<Character>();
            try
            {
                // select query from the table in the database
                string queryString = $"SELECT * FROM {table}";
                // open the database connection 
                using (SqlConnection sqlConnection = new SqlConnection(ConnectionString()))
                {
                    SqlCommand command = new SqlCommand(queryString, sqlConnection);
                    sqlConnection.Open();
                    // while the connection is open, read from the database
                    using (SqlDataReader reader = command.ExecuteReader()) 
                    {
                        while (reader.Read())
                        {
                            // character variable sets to empty
                            Character character = null;
                            // gets the value of a row based on the column index
                            int id = reader.GetInt32(0);
                            string name = reader.GetString(1);

                            switch (table)
                            {
                                // Types of thieves
                                case "Assassain":
                                    character = new Assassain(name);
                                    break;
                                case "Bandit":
                                    character = new Bandit(name);
                                    break;
                                case "Rogue":
                                    character = new Rogue(name);
                                    break;
                                // Types of warriors
                                case "Knight":
                                    character = new Knight(name);
                                    break;
                                case "Samurai":
                                    character = new Samurai(name);
                                    break;
                                case "Viking":
                                    character = new Viking(name);
                                    break;
                                // Types of wizards
                                case "Druid":
                                    character = new Druid(name);
                                    break;
                                case "Shaman":
                                    character = new Shaman(name);
                                    break;
                                case "Warlock":
                                    character = new Warlock(name);
                                    break;
                            }
                            character.Health = reader.GetInt32(2);
                            character.Intellect = reader.GetInt32(3);
                            character.Energy = reader.GetInt32(4);
                            character.Stamina = reader.GetInt32(5);
                            character.Agility = reader.GetInt32(6);
                            character.Armor = reader.GetInt32(7);
                            character.HitPoints = reader.GetInt32(8);

                            if (character != null)
                            {
                                // if the character is not null, set the character Id and add it to the item list
                                character.ID = id;

                                items.Add(character);
                            }
                        }
                    }
                    sqlConnection.Close();
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return items;
        }

    #endregion
       

        #region Update
        public static string Update(string table, Character character)
        {
            // update query
            string query = $"UPDATE {table} SET Name = @name, Health = @hp, Intellect = @int, Energy = @eng, Stamina = @sta, Agility = @agi, Armor = @arm, HitPoints = @hit WHERE ID = @id";

            using (SqlConnection connection = new SqlConnection(ConnectionString()))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    //update the parameters of the query in the database 
                    // @name, @hp, @int, @eng, @sta, @agi, @arm, @hit
                    cmd.Parameters.AddWithValue("@id", character.ID);
                    cmd.Parameters.AddWithValue("@name", character.Name);
                    cmd.Parameters.AddWithValue("@hp", character.Health);
                    cmd.Parameters.AddWithValue("@int", character.Intellect);
                    cmd.Parameters.AddWithValue("@eng", character.Energy);
                    cmd.Parameters.AddWithValue("@sta", character.Stamina);
                    cmd.Parameters.AddWithValue("@agi", character.Agility);
                    cmd.Parameters.AddWithValue("@arm", character.Armor);
                    cmd.Parameters.AddWithValue("@hit", character.HitPoints);

                    cmd.ExecuteNonQuery();
                }

                connection.Close();
            }

            return query;
        }

        #endregion


        #region Delete 
        public static void Delete(string table, Character character)
        {
            // delete query
            string query = $"DELETE FROM {table} WHERE ID = @id";

            using (SqlConnection connection = new SqlConnection(ConnectionString()))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    // delete the row where the ID matches in the database
                    // @name, @hp, @int, @eng, @sta, @agi, @arm, @hit
                    cmd.Parameters.AddWithValue("@id", character.ID);

                    cmd.ExecuteNonQuery();
                }

                connection.Close();
            }
        }
        #endregion

    }
}
