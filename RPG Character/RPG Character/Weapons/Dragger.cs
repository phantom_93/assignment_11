﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Weapons
{
    public class Dragger: Weapon
    {
        #region constructor 
        public Dragger(string name) : base(name)
        {

        }
        public Dragger(string name, int damage) : base(name, damage)
        {

        }
        #endregion
    }
}
