﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Weapons
{
    public class MagicWand: Weapon
    {
        #region constructor
        public MagicWand(string name) : base(name)
        {

        }
        public MagicWand(string name, int damage) : base(name, damage)
        {

        }
        #endregion
    }
}
