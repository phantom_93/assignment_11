﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Weapons
{
    public abstract class Weapon : Entity
    {
        #region properties
        public int ID { get; set; }
        public int Damage { get; set; }
        
        public int Capability { get; set; }
        #endregion

        public static Type[] DefinedWeapons =
        {
            typeof(ThrowingStar),
            typeof(Dragger),
            typeof(Sword),
            typeof(Poison),
            typeof(MagicWand)
        };

        #region constructor
        public Weapon(string name)
            :base(name)
        {
            
        }

        public Weapon(string name, int damage)
            : base(name)
        {
            Damage = damage;
        }


        public Weapon(string name, int damage, int capability)
            : base(name)
        {
            Damage = damage;
            Capability = capability; 

        }

        #endregion

       
        #region methods
        public string GetWeaponType()
        {
            Type type = GetType();
            string[] namespaceAndType = type.ToString().Split('.');
            string charType = namespaceAndType[namespaceAndType.Length - 1];
            return charType;

        }
        public override string ToString()
        {
            return $"Id: {ID}, Name: {Name}, Damage: {Damage}, Capability: {Capability}";
        }
        #endregion
    }
}
