﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Weapons
{
    #region constructor
    public class ThrowingStar: Weapon
    {
        public ThrowingStar(string name) : base(name)
        {

        }

        public ThrowingStar(string name, int damage):base(name, damage)
        {

        }
    }
    #endregion
}
