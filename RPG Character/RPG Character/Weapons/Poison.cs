﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Weapons
{
    #region constructor
    public class Poison: Weapon
    {
        public Poison(string name) : base(name)
        {

        }

        public Poison(string name, int damage) : base(name, damage)
        {

        }
    }
    #endregion
}
