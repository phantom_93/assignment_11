﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character.Weapons
{
    #region constructor 
    public class Sword: Weapon
    {
        public Sword(string name) : base(name)
        {

        }

        public Sword(string name, int damage) : base(name, damage)
        {

        }
    }
    #endregion
}
