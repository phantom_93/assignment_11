﻿using RPG_Character.Characters;
using RPG_Character.Weapons;
using System;

namespace PlayConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // a short demo for gaming simulation
            // all interfaces methods are used in this console to simulate 
            #region createObjects for round 1
            Sword sword = new Sword("ThiefLegend", 20);
            Rogue techleadOttosen = new Rogue("techleadOttosen", sword);
         

            Sword sword1 = new Sword("GreatSword", 100);
            Assassain nicholas = new Assassain("Nicholas", sword1);
            #endregion


            #region createOjects for round 2 
            Sword sword2 = new Sword("Sky Sword", 100);
            Samurai nicolas = new Samurai("Nicolas", sword2);

            Sword sword3 = new Sword("fire Sword", 100);
            Knight john = new Knight("John", sword3);
            #endregion


            #region createObjects for round 3 
            MagicWand wand = new MagicWand("dragon", 100);
            Warlock jack = new Warlock("Jack", wand);

            MagicWand wand1 = new MagicWand("pheonix", 100);
            Druid rambo  = new Druid("Jack", wand);
            #endregion



            #region round 1
            // battle among thieves
            Console.WriteLine("\nRound 1");
            techleadOttosen.Attack(nicholas,sword);
            nicholas.Kill(techleadOttosen, sword1);
            Console.WriteLine("*******************");
            #endregion


            #region round 2
            // battle among warroris 
            Console.WriteLine("\nRound 2");
            john.Attack(nicolas, sword3);
            nicolas.Block(john, sword2);
            nicolas.Kill(john, sword2);
            Console.WriteLine("*******************");
            #endregion


            #region round 3 
            // battle among wizards 
            Console.WriteLine("\nRound 3");
            jack.Manipulate(rambo, wand);
            rambo.Kill(jack, wand1); 
            #endregion


        }
    }
}
