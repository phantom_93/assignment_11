USE [master]
GO
/****** Object:  Database [RPG_Character]    Script Date: 8/26/2020 3:37:59 PM ******/
CREATE DATABASE [RPG_Character]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RPG_Character', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\RPG_Character.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'RPG_Character_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\RPG_Character_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [RPG_Character] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RPG_Character].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RPG_Character] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RPG_Character] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RPG_Character] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RPG_Character] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RPG_Character] SET ARITHABORT OFF 
GO
ALTER DATABASE [RPG_Character] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RPG_Character] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RPG_Character] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RPG_Character] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RPG_Character] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RPG_Character] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RPG_Character] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RPG_Character] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RPG_Character] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RPG_Character] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RPG_Character] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RPG_Character] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RPG_Character] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RPG_Character] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RPG_Character] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RPG_Character] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RPG_Character] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RPG_Character] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RPG_Character] SET  MULTI_USER 
GO
ALTER DATABASE [RPG_Character] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RPG_Character] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RPG_Character] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RPG_Character] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [RPG_Character] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [RPG_Character] SET QUERY_STORE = OFF
GO
USE [RPG_Character]
GO
/****** Object:  Table [dbo].[Assassain]    Script Date: 8/26/2020 3:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assassain](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Health] [int] NULL,
	[Intellect] [int] NULL,
	[Energy] [int] NULL,
	[Stamina] [int] NULL,
	[Agility] [int] NULL,
	[Armor] [int] NULL,
	[HitPoints] [int] NULL,
	[WeaponID] [int] NULL,
 CONSTRAINT [PK_Assassain] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bandit]    Script Date: 8/26/2020 3:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bandit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Health] [int] NULL,
	[Intellect] [int] NULL,
	[Energy] [int] NULL,
	[Stamina] [int] NULL,
	[Agility] [int] NULL,
	[Armor] [int] NULL,
	[HitPoints] [int] NULL,
	[WeaponID] [int] NULL,
 CONSTRAINT [PK_Bandit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Druid]    Script Date: 8/26/2020 3:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Druid](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Health] [int] NULL,
	[Intellect] [int] NULL,
	[Energy] [int] NULL,
	[Stamina] [int] NULL,
	[Agility] [int] NULL,
	[Armor] [int] NULL,
	[HitPoints] [int] NULL,
	[WeaponID] [int] NULL,
 CONSTRAINT [PK_Druid] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Knight]    Script Date: 8/26/2020 3:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Knight](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Health] [int] NULL,
	[Intellect] [int] NULL,
	[Energy] [int] NULL,
	[Stamina] [int] NULL,
	[Agility] [int] NULL,
	[Armor] [int] NULL,
	[HitPoints] [int] NULL,
	[WeaponID] [int] NULL,
 CONSTRAINT [PK_Knight] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rogue]    Script Date: 8/26/2020 3:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rogue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Health] [int] NULL,
	[Intellect] [int] NULL,
	[Energy] [int] NULL,
	[Stamina] [int] NULL,
	[Agility] [int] NULL,
	[Armor] [int] NULL,
	[HitPoints] [int] NULL,
	[WeaponID] [int] NULL,
 CONSTRAINT [PK_Rogue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Samurai]    Script Date: 8/26/2020 3:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Samurai](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Health] [int] NULL,
	[Intellect] [int] NULL,
	[Energy] [int] NULL,
	[Stamina] [int] NULL,
	[Agility] [int] NULL,
	[Armor] [int] NULL,
	[HitPoints] [int] NULL,
	[WeaponID] [int] NULL,
 CONSTRAINT [PK_Samurai] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shaman]    Script Date: 8/26/2020 3:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shaman](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Health] [int] NULL,
	[Intellect] [int] NULL,
	[Energy] [int] NULL,
	[Stamina] [int] NULL,
	[Agility] [int] NULL,
	[Armor] [int] NULL,
	[HitPoints] [int] NULL,
	[WeaponID] [int] NULL,
 CONSTRAINT [PK_Shaman] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Viking]    Script Date: 8/26/2020 3:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Viking](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Health] [int] NULL,
	[Intellect] [int] NULL,
	[Energy] [int] NULL,
	[Stamina] [int] NULL,
	[Agility] [int] NULL,
	[Armor] [int] NULL,
	[HitPoints] [int] NULL,
	[WeaponID] [int] NULL,
 CONSTRAINT [PK_Viking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warlock]    Script Date: 8/26/2020 3:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warlock](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Health] [int] NULL,
	[Intellect] [int] NULL,
	[Energy] [int] NULL,
	[Stamina] [int] NULL,
	[Agility] [int] NULL,
	[Armor] [int] NULL,
	[HitPoints] [int] NULL,
	[WeaponID] [int] NULL,
 CONSTRAINT [PK_Warlock] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Weapons]    Script Date: 8/26/2020 3:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Weapons](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Damage] [int] NOT NULL,
	[Capability] [int] NOT NULL,
 CONSTRAINT [PK_Weapons] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Assassain] ON 

INSERT [dbo].[Assassain] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (3, N'Bradley Cooper', 100, 100, 100, 100, 100, 100, 100, NULL)
INSERT [dbo].[Assassain] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (4, N'CreateTest2', 100, 100, 100, 100, 100, 100, 100, NULL)
INSERT [dbo].[Assassain] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (5, N'CreateTest3', 100, 100, 100, 100, 100, 100, 100, NULL)
SET IDENTITY_INSERT [dbo].[Assassain] OFF
GO
SET IDENTITY_INSERT [dbo].[Bandit] ON 

INSERT [dbo].[Bandit] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (1, N'ghost', 34, 36, 89, 12, 78, 76, 34, NULL)
INSERT [dbo].[Bandit] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (2, N'Phantom', 100, 100, 100, 100, 100, 100, 100, NULL)
SET IDENTITY_INSERT [dbo].[Bandit] OFF
GO
SET IDENTITY_INSERT [dbo].[Druid] ON 

INSERT [dbo].[Druid] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (1, N'mathiastb', 100, 100, 100, 100, 100, 100, 100, NULL)
INSERT [dbo].[Druid] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (2, N'nicholas94', 98, 98, 98, 98, 98, 98, 98, NULL)
SET IDENTITY_INSERT [dbo].[Druid] OFF
GO
SET IDENTITY_INSERT [dbo].[Knight] ON 

INSERT [dbo].[Knight] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (1, N'Test Knight', 100, 100, 100, 100, 100, 100, 100, NULL)
INSERT [dbo].[Knight] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (2, N'Templar', 67, 67, 76, 76, 45, 45, 78, NULL)
SET IDENTITY_INSERT [dbo].[Knight] OFF
GO
SET IDENTITY_INSERT [dbo].[Rogue] ON 

INSERT [dbo].[Rogue] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (1, N'Jack', 100, 100, 100, 100, 100, 100, 100, NULL)
INSERT [dbo].[Rogue] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (2, N'percey', 45, 78, 87, 78, 98, 65, 78, NULL)
SET IDENTITY_INSERT [dbo].[Rogue] OFF
GO
SET IDENTITY_INSERT [dbo].[Samurai] ON 

INSERT [dbo].[Samurai] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (1, N'testSamurai1', 100, 100, 100, 100, 100, 100, 100, NULL)
INSERT [dbo].[Samurai] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (2, N'SamuraiX', 98, 98, 98, 98, 98, 98, 98, NULL)
SET IDENTITY_INSERT [dbo].[Samurai] OFF
GO
SET IDENTITY_INSERT [dbo].[Shaman] ON 

INSERT [dbo].[Shaman] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (1, N'shax', 100, 100, 100, 89, 86, 87, 34, NULL)
INSERT [dbo].[Shaman] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (2, N'shay', 45, 98, 89, 56, 56, 45, 98, NULL)
SET IDENTITY_INSERT [dbo].[Shaman] OFF
GO
SET IDENTITY_INSERT [dbo].[Viking] ON 

INSERT [dbo].[Viking] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (1, N'Adam', 100, 100, 100, 100, 100, 100, 100, NULL)
INSERT [dbo].[Viking] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (2, N'Andre', 76, 78, 90, 67, 56, 35, 89, NULL)
SET IDENTITY_INSERT [dbo].[Viking] OFF
GO
SET IDENTITY_INSERT [dbo].[Warlock] ON 

INSERT [dbo].[Warlock] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (2, N'warlockX', 100, 100, 100, 100, 100, 100, 100, NULL)
INSERT [dbo].[Warlock] ([ID], [Name], [Health], [Intellect], [Energy], [Stamina], [Agility], [Armor], [HitPoints], [WeaponID]) VALUES (3, N'warlockY', 46, 34, 56, 37, 19, 27, 38, NULL)
SET IDENTITY_INSERT [dbo].[Warlock] OFF
GO
SET IDENTITY_INSERT [dbo].[Weapons] ON 

INSERT [dbo].[Weapons] ([ID], [Name], [Type], [Damage], [Capability]) VALUES (13, N'droggo', N'Dragger', 100, 100)
INSERT [dbo].[Weapons] ([ID], [Name], [Type], [Damage], [Capability]) VALUES (16, N'Killer', N'ThrowingStar', 100, 100)
INSERT [dbo].[Weapons] ([ID], [Name], [Type], [Damage], [Capability]) VALUES (17, N'KingSword', N'Sword', 100, 100)
INSERT [dbo].[Weapons] ([ID], [Name], [Type], [Damage], [Capability]) VALUES (18, N'kissofDeath', N'Poison', 100, 100)
INSERT [dbo].[Weapons] ([ID], [Name], [Type], [Damage], [Capability]) VALUES (19, N'kissMyFeet', N'MagicWand', 100, 100)
SET IDENTITY_INSERT [dbo].[Weapons] OFF
GO
ALTER TABLE [dbo].[Assassain]  WITH CHECK ADD  CONSTRAINT [FK_Assassain_Weapons] FOREIGN KEY([WeaponID])
REFERENCES [dbo].[Weapons] ([ID])
GO
ALTER TABLE [dbo].[Assassain] CHECK CONSTRAINT [FK_Assassain_Weapons]
GO
ALTER TABLE [dbo].[Bandit]  WITH CHECK ADD  CONSTRAINT [FK_Bandit_Weapons] FOREIGN KEY([WeaponID])
REFERENCES [dbo].[Weapons] ([ID])
GO
ALTER TABLE [dbo].[Bandit] CHECK CONSTRAINT [FK_Bandit_Weapons]
GO
ALTER TABLE [dbo].[Druid]  WITH CHECK ADD  CONSTRAINT [FK_Druid_Weapons] FOREIGN KEY([WeaponID])
REFERENCES [dbo].[Weapons] ([ID])
GO
ALTER TABLE [dbo].[Druid] CHECK CONSTRAINT [FK_Druid_Weapons]
GO
ALTER TABLE [dbo].[Knight]  WITH CHECK ADD  CONSTRAINT [FK_Knight_Weapons] FOREIGN KEY([WeaponID])
REFERENCES [dbo].[Weapons] ([ID])
GO
ALTER TABLE [dbo].[Knight] CHECK CONSTRAINT [FK_Knight_Weapons]
GO
ALTER TABLE [dbo].[Rogue]  WITH CHECK ADD  CONSTRAINT [FK_Rogue_Weapons] FOREIGN KEY([WeaponID])
REFERENCES [dbo].[Weapons] ([ID])
GO
ALTER TABLE [dbo].[Rogue] CHECK CONSTRAINT [FK_Rogue_Weapons]
GO
ALTER TABLE [dbo].[Samurai]  WITH CHECK ADD  CONSTRAINT [FK_Samurai_Weapons] FOREIGN KEY([WeaponID])
REFERENCES [dbo].[Weapons] ([ID])
GO
ALTER TABLE [dbo].[Samurai] CHECK CONSTRAINT [FK_Samurai_Weapons]
GO
ALTER TABLE [dbo].[Shaman]  WITH CHECK ADD  CONSTRAINT [FK_Shaman_Weapons] FOREIGN KEY([WeaponID])
REFERENCES [dbo].[Weapons] ([ID])
GO
ALTER TABLE [dbo].[Shaman] CHECK CONSTRAINT [FK_Shaman_Weapons]
GO
ALTER TABLE [dbo].[Viking]  WITH CHECK ADD  CONSTRAINT [FK_Viking_Weapons] FOREIGN KEY([WeaponID])
REFERENCES [dbo].[Weapons] ([ID])
GO
ALTER TABLE [dbo].[Viking] CHECK CONSTRAINT [FK_Viking_Weapons]
GO
ALTER TABLE [dbo].[Warlock]  WITH CHECK ADD  CONSTRAINT [FK_Warlock_Weapons] FOREIGN KEY([WeaponID])
REFERENCES [dbo].[Weapons] ([ID])
GO
ALTER TABLE [dbo].[Warlock] CHECK CONSTRAINT [FK_Warlock_Weapons]
GO
USE [master]
GO
ALTER DATABASE [RPG_Character] SET  READ_WRITE 
GO
