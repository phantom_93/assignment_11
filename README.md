Assignment 11 - RPG Character Generator
-----------------------------------------------------------------------------------
Type:    Windows Form, WPF or UWP

Allows a user to create an RPG character

Requirements for PART 1:

·         A character can be a Wizard, Warrior, Thief (Or choose your own)

·         Each character type can have subtypes (be creative)

·         A character has HP, Mana/Energy, Name, armorRating

·         A character can attack and move

·         Create a form to allow for creation of the above character/s

·         Once the user clicks create/submit, a summary of the character must be displayed

·         This character must be created as an object and that object must be used to display the summary as well as write the details to a text file

Additional Information
Create the class structure first, in a separate project
Use the built DLL to create objects, therefore build your UI according to their class structure
Either create a class library (.NET framework) and a console application in one solution OR create a console application then pull the DLL from the bin folder
Purpose: Learn C#: External Libraries, File and IO, DotNet GUI and Event Driven System

Weight: Critical



PART 2 additions:
•Upgrade your RPG Character Generator to allows a user to create an RPG character and store it in a database
Requirements:
•Create a database with appropriate table to store data for the RPG Character objects
•Change the text writing code to generate a valid INSERT sql query that you can then be executed in DB Browser to add a character to the table (in place of a character summary)
•Purpose: Learn SQL: SQLServer and File IO



Part 3 additions:
Upgrade RPG Character creator

•Allows a user to create an RPG character and store it in a database. Additionally, allow for Deleting, Inserting, Updating and Displaying characters to and from the database using objects
•Requirements:
•A Connection manager must be used to perform CRUD operations on RPG objects.
•Purpose: Learn C# and SQL: Database Connection Manager


----------------------------------------------------------------------------------------------------------------------------------
for this assignment, the following packages are downloaded through microsoft NuGet Package Manager
 
1. CsvHelper 
2. Microsoft.EntityFrameworkCore.Design
3. System.Data.SqlClient
these packages are used through RPG Character Library and that is used as dll(reference)in RPG character GUI. 


PlayConsole: use interfaces in order to demonstrate a simulation of game characters seperately. 

RPG Character GUI does other requirments that the assignment asks for. 









